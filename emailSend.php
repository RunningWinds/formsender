<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require './vendor/autoload.php';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->isSMTP();
    $mail->Host       = 'smtp.gmail.com';
    $mail->SMTPAuth   = true;
    $mail->Username   = 'здесь ваш адрес гугл';
    $mail->Password   = 'здесь ваш пароль приложения';
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    $mail->Port       = 465;
    $subject = '';
    $body = '';

    //Recipients
    $mail->setFrom('hr@tzspeakenglish.com', 'TzSpeakEnglish');
    $mail->addAddress('velt-nikita@yandex.ru', 'EsteLab');

    //Content
    $mail->isHTML(true);
    if ((isset($_POST['name']))&&(isset($_POST['phone']))){
        $subject = 'Форма по ТЗ';
        $body = 'Имя: '.$_POST['name'].'<br> Телефон: '.$_POST['phone'];

        if (isset($_POST['question'])){
            $body .= $mail->Body . '<br> Вопрос: '.$_POST['question'];
        }
    } 
    $mail->Subject = "=?UTF-8?B?".base64_encode($subject)."?=";
    $mail->Body = func_сyrillic_html_encode($body);
    $mail->send();
    echo 'ok';
}
 catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}


function func_сyrillic_html_encode($text)  {
    $patterns = preg_split("/(?<!^)(?!$)/u", "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя");
    $html = array();
    foreach ($patterns as $i => $letter) {
      $patterns[$i] = "/$letter/u";
      $html[] = "&#".($i+1040).";";
    }		
    return preg_replace($patterns, $html, $text);
  }