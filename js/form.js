async function sendForm(form,dest) {

    let response = await fetch(dest, {

        method: "POST",

        body: new FormData(form)

    });

    let result = await response.text();

    if (result == "ok") {

        closeModalForm();

    }

}



function openModalForm() {

    $("#overlay").fadeIn(1000);

    $("#modal").fadeIn(1000);

}



function closeModalForm() {

    $("#overlay").fadeOut(1000);

    $("#modal").fadeOut(1000);

}